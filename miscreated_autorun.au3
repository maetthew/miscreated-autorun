#include <Misc.au3>

Const $windowName = "Miscreated"
Const $user32 = DllOpen("user32.dll")

; Name/location of config file
Const $configFile = "config.ini"

; Set up variables
Local $Hotkey
Local $StopKeys = ["0D", "09", "1B", "47", "53", "58"] ; Keys to look for and stop running: Enter, TAB, ESC, G, S keys (We also stop with "W" but that happens elsewhere)
Local $GameForwardKey = "w" ; Key that is used to move forward in game
Local $GameRunModifierKey = "LSHIFT" ; Key that is used to toggle run or walk
Local $GameClSprintToggle ; If key toggle on $GameRunModifierKey is activated in game
Local $StatusToolTip

Local $Running = False ; Variable for run state. Assume that we are currently not running when script is started

LoadConfig() ; Load or create a config

OnAutoItExitRegister("StopRunning") ; Release runkeys if application exits

; Set up tray
AutoItSetOption("TrayMenuMode", 3)
TraySetToolTip("Miscreated Autorun")
Local $TrayReload = TrayCreateItem("Reload config")
TrayCreateItem("")
Local $TrayExit = TrayCreateItem("Exit")

While True
	If _IsPressed($Hotkey, $user32) And WinActive($windowName) Then ; If our hotkey is pressed and the game is the active window
		While _IsPressed($Hotkey, $user32) ; Wait for release
			Sleep(100)
		WEnd

		If Not $Running Then ; Check whether we are currently running or not, and proceed accordingly
			StartRunning()
		ElseIf $Running Then
			StopRunning()
		EndIf
	EndIf

	If $Running And WinActive($windowName) Then ; If we are currently running and game is the active window, check if any of the keys in $StopKeys is pressed
		For $key in $StopKeys ; Iterate through each key in $StopKeys
			If _IsPressed($key, $user32) Then ; Check if any of them are pressed
				While _IsPressed($key, $user32) ; Wait for release
					Sleep(50)
				WEnd

				StopRunning() ; Stop running
			EndIf
		Next

		If Not _IsPressed("57", $user32) Then ; If W is released (e.g manually pressed), stop running
			StopRunning()
		EndIf
	EndIf

	If $Running And Not WinActive($windowName) Then StopRunning() ; If for some reason our game window is no longer the active one, stop running
	If $Running And Not ProcessExists("Miscreated.exe") Then StopRunning() ; If our game suddenly crashes or exits, stop running

	If $StatusToolTip = 1 Then ; If ToolTip=1 in config.ini, show a tooltip
		If $Running Then
			ToolTip("Y", 0, 0)
		ElseIf Not $Running Then
			ToolTip("N", 0, 0)
		EndIf
	EndIf

	Switch TrayGetMsg()
		Case $TrayReload
			LoadConfig()
		Case $TrayExit
			ExitLoop
	EndSwitch

	Sleep(100)
WEnd

Func StartRunning()
	If Send("{" & $GameForwardKey & " down}" & "{" & $GameRunModifierKey & " down}") Then ToggleRunning() ; Press W+LSHIFT and toggle the running state

	If $GameClSprintToggle = 1 Then ; If sprint toggle is activated in game, release run modifier key after 100ms
		Sleep(100)
		If Send("{" & $GameRunModifierKey & " up}") Then ConsoleWrite("* Released " & $GameRunModifierKey & @CRLF)
	EndIf
EndFunc

Func StopRunning()
	If $Running Then
		If Send("{" & $GameForwardKey & " up}" & "{" & $GameRunModifierKey & " up}") Then ToggleRunning() ; Release W+LSHIFT
	EndIf
EndFunc

Func ToggleRunning()
	$Running = Not $Running
	ConsoleWrite("Running: " & $Running & @CRLF)
EndFunc

Func LoadConfig() ; Very basic config
	If Not FileExists($configFile) Then
		IniWrite($configFile, "Miscreated", "Hotkey", "06")
		IniWrite($configFile, "Miscreated", "ToolTip", "0")
		IniWrite($configFile, "Miscreated", "cl_sprintToggle", "0")
		ConsoleWrite("Config created" & @CRLF)
		LoadConfig()
	Else
		$Hotkey = IniRead($configFile, "Miscreated", "Hotkey", "NO VALUE")
		$GameClSprintToggle = IniRead($configFile, "Miscreated", "cl_sprintToggle", "NO VALUE")
		$StatusToolTip = IniRead($configFile, "Miscreated", "ToolTip", "NO VALUE")
		ConsoleWrite("Config loaded" & @CRLF)
	EndIf
EndFunc