# Miscreated autorun #

Autorun script for the game Miscreated with cl_sprintToggle support.

## How to use ##
On start of the script it will check if config.ini exists. If it doesnt, it will create a default config. Edit the config and restart the script or chose "Reload config" from tray menu (this is slow for some reason, but works. Give it 1-5s after you "Reload config").

Press hotkey while in game to start running. While running the script will stop running if you press any of the following keys: TAB, Enter, ESC, W, S, G

### The config ###

#### Hotkey = 06 ####
Use corresponding code for your key on the list on [this webpage](https://www.autoitscript.com/autoit3/docs/libfunctions/_IsPressed.htm). Default is "06" for MBUTTON5. Do not use W or LSHIFT.

#### ToolTip = 0 ####
##### Can be 0 or 1 #####
If you want to see if the script "thinks" we are autorunning or not. This will display a small tool tip in the top left corner of your screen with "Y" or "N". If there is no tooltip, script thinks we are not autorunning.

#### cl_sprintToggle = 0 ####
##### Can be 0 or 1 ######
Use the same value as in Miscreated's system.cfg. If you have no cl_sprintToggle in system.cfg, use 0. If 1 it will release LSHIFT 100ms after we start auto running, if 0 it will keep W+LSHIFT pressed until you stop it.

### Be aware ###

This script has no way of knowing if you are actually running in-game or not, it doesn't get any information from Miscreated. It only checks if the string "Miscreated" is in the currently active window's title on your computer. When the script is started, it's assumed we are not running. Press your hot key and and the script will press down W+LSHIFT. This might lead to the script executing in some scenarios which are unwanted, some examples of scenarios:

* If you have the chatbox open in game and press the hotkey, the script will execute (press down W+LSHIFT)
* If you die while auto running, W+LSHIFT will be kept pressed down until you press your hotkey or one of the stop keys.
* If you or any other process on your computer for any reason takes the "focus" from your game window, W+LSHIFT will still be pressed down (and you have to either exit script or press on of the stop keys or the hotkey with Miscreated active/focused)